# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Funcionamiento básico de filtros en ElasticSearch

Ejemplo de cómo filtrar documentos en ES, mostrando la diferencia entre una consulta tipo `match` (búsqueda exacta) o `fuzzy` (búsqueda similar).


# Instalación
-----------------------

La primera vez ejecutar:

```
$ make install
```

Subsiguientes veces para seguir practicando con ElasticSearch basta hacer:
```
$ make up
```



# Instrucciones
-----------------------

- Ejecuta `make install` en la raíz del repositorio
- Mira dentro de  `./src/examples` cada uno de los ejemplos, pudiendo cambiar los datos en ellos. Puedes ejecutarlos para ver tanto la petición hecha a ES, así como la respuesta.
- Ve ejecutando secuencialmente los diferentes ejemplos:
	- docker-compose run --rm php php /root/src/examples/01_insert_documents.php
	- docker-compose run --rm php php /root/src/examples/02_get_document.php
	- docker-compose run --rm php php /root/src/examples/03_search_document.php
	- docker-compose run --rm php php /root/src/examples/04_exceptions.php

- Inserta varios documentos en ES mediante la ejecución del `01_insert_documents.php` y mira como la versión aumenta (campo "_version")	
- Entra en la URL 'http://localhost:8080/' para ver un formulario para buscar nombres de cuentas.
- Busca los clientes con nombre `martin`
- Puedes ver en la parte inferior de la página la consulta que se ha lanzado contra ES
- Ahora descomenta la siguiente línea del fichero `results_table.php`: `//$matchType = new SearchFuzzyMatch();`
- Vuelve a buscar los clientes con nombre `martin`


# Posibles problemas
---------------------

Si aparece el siguiente error al ejecutar `make install` o `make populate` significa que ElasticSearch todavía no estaba
preparado para recibir conexiones para hacer la carga de datos inicial. Reintenta ejecutar `make populate` pasado un minuto.

```
curl es:9200
curl: (7) Failed to connect to es port 9200: Connection refused
```

# Kibana

Como alternativa al plugin de Sensu, se puede usar Kibana. Para acceder a Kibana usar un navegador y introducir la IP http://localhost:5601/

Kibana es una herramienta muy interesante de visualización y generación de Dashboards, pero en el caso que nos ocupa usaremos el apartado "Dev Tools".
Éste nos permite crear consultas tales como:

Listar todos los índices disponibles en ElastisSearch
```
GET /_cat/indices
```

Buscar documentos en uno de los índices (funcionará siempre que esté creado):
```
GET /bank/_search
```



# Desinstalación
-----------------------

```
$ make down
```
