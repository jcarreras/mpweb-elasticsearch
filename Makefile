install: up vendor populate

vendor:
	docker-compose run --rm composer composer install --ignore-platform-reqs

populate:
	docker-compose run --rm composer curl -XPOST -H "Content-Type: application/x-ndjson" 'es:9200/bank/account/_bulk?pretty&refresh' --data-binary "@accounts.json"

up:
	docker-compose up -d

down:
	docker-compose down
