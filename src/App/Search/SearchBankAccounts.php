<?php namespace App\Search;


class SearchBankAccounts extends Search
{

	protected function getIndex()
	{
		return 'bank';
	}

	protected function getType()
	{
		return 'account';
	}
}