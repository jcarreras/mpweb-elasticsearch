<?php namespace App\Search;

use Elasticsearch\Client;


abstract class Search
{
	/** @var ClientBuilder */
	private $client;
	/** @var SearchQuery */
	private $searchQuery;

	protected abstract function getIndex();

	protected abstract function getType();

	/**
	 * Search constructor.
	 * @param Client $client
	 * @param SearchQuery $searchQuery
	 */
	public function __construct(Client  $client, SearchQuery $searchQuery)
	{
		$this->client      = $client;
		$this->searchQuery = $searchQuery;
	}

	public function search()
	{
		$params = [
			'index' => $this->getIndex(),
			'type'  => $this->getType(),
			'body'  => [
				'query' => $this->searchQuery->getFilter(),
			],
		];

		return $this->client->search($params);
	}
}